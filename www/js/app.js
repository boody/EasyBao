// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers', 'starter.services', 'starter.directive'])

.run(function($ionicPlatform, $ionicPopup) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    if(window.Connection) {
      if(navigator.connection.type == Connection.NONE) {
          $ionicPopup.confirm({
            title: "网络错误",
            content: "设备无法查找到可用网络."
          })
          .then(function(result) {
            if(!result) {
              ionic.Platform.exitApp();
            }
          });
      }
    }
  });
})

.config(function($ionicConfigProvider) {
  $ionicConfigProvider.platform.android.tabs.style('standard');
  $ionicConfigProvider.platform.android.tabs.position('bottom');
  $ionicConfigProvider.platform.android.navBar.alignTitle('center');
  $ionicConfigProvider.platform.android.views.transition('android');
})

.config(function($httpProvider) {
  //$httpProvider.defaults.timeout = 5000;
  $httpProvider.interceptors.push(function($rootScope, $q) {
    return {
      request: function(config) {
        config.timeout = 3000;
        $rootScope.$broadcast('loading:show');
        return config;
      },
      response: function(response) {
        $rootScope.$broadcast('loading:hide');
        return response;
      },
      requestError: function(response) {
        $rootScope.$broadcast('loading:hide');
        return $q.reject(response);
      },
      responseError: function(response) {
        $rootScope.$broadcast('loading:hide');
        return $q.reject(response);
      }
    }
  })
})

.run(function($rootScope, $ionicLoading) {
  $rootScope.$on('loading:show', function() {
    $ionicLoading.show({template: '<ion-spinner></ion-spinner>'});
  })

  $rootScope.$on('loading:hide', function() {
    $ionicLoading.hide();
  })
})

.config(function($stateProvider, $urlRouterProvider) {
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/apps.html',
    controller: 'TabCtrl'
  })

  // Each tab has its own nav history stack:

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl'
  })

//    .state('orders', {
//    url: '/orders',
//    templateUrl: 'templates/orders.html',
//    controller: 'OrderCtrl'
//  })

  .state('app.repairs', {
    url: '/repairs',
    views: {
      'app-repairs': {
        templateUrl: 'templates/app-repairs.html',
        controller: 'RepairsCtrl'
      }
    }
  })

  .state('app.repair-detail', {
    url: '/repairs/:repairId',
    views: {
      'app-repairs': {
        templateUrl: 'templates/repair-detail.html',
        controller: 'RepairDetailCtrl'
      }
    }
  })

  .state('app.home', {
    url: '/home',
    views: {
      'app-home': {
        templateUrl: 'templates/app-home.html',
        controller: 'HomeCtrl'
      }
    }
  })

  .state('app.chats', {
    url: '/chats',
    views: {
      'app-chats': {
        templateUrl: 'templates/app-chats.html',
        controller: 'ChatsCtrl'
      }
    }
  })

  .state('app.chat-detail', {
    url: '/chats/:chatId',
    views: {
      'app-chats': {
        templateUrl: 'templates/chat-detail.html',
        controller: 'ChatDetailCtrl'
      }
    }
  })

  .state('app.account', {
    url: '/account',
    views: {
      'app-account': {
        templateUrl: 'templates/app-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});
