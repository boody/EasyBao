angular.module('starter.directive', [])

.directive("appMap", function () {
  return {
    restrict: "E",
    replace: true,
    template: "<div id='allMap'></div>",
    scope: {
      center: "=",        // Center point on the map (e.g. <code>{ latitude: 10, longitude: 10 }</code>).
      markers: "=",       // Array of map markers (e.g. <code>[{ lat: 10, lon: 10, name: "hello" }]</code>).
      width: "@",         // Map width in pixels.
      height: "@",        // Map height in pixels.
      zoom: "@",          // Zoom level (one is totally zoomed out, 25 is very much zoomed in).
      zoomControl: "@",   // Whether to show a zoom control on the map.
      scaleControl: "@",   // Whether to show scale control on the map.
      address:"@"
    },
    link: function (scope, element, attrs) {
      console.log('Baidu Map...');
//      navigator.geolocation.getCurrentPosition(function(pos){
//        alert(JSON.stringify(pos));
//          var currentLat = pos.coords.latitude;
//          var currentLon = pos.coords.longitude;
//          var gpsPoint = new BMap.Point(currentLon, currentLat);
//          BMap.Convertor.translate(gpsPoint, 0, function(point) {
//            // 百度地图API功能
//            var map = new BMap.Map("allMap");
//            //var point = new BMap.Point(115.949652,28.693851);
//            map.centerAndZoom(point,18);//
//            //向地图中添加缩放控件
//            var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
//            map.addControl(ctrl_nav);
//            //向地图中添加缩略图控件
//            var ctrl_ove = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:1});
//            map.addControl(ctrl_ove);
//            //向地图中添加比例尺控件
//            var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
//            map.addControl(ctrl_sca);
//            map.addOverlay(new BMap.Marker(point));
//          });
//      },function(err){
//        alert(JSON.stringify(err));
//      }, {
//        enableHighAccuracy: false,
//        timeout: 6*1000,
//        maximumAge: 1000*60*10
//      });

				baidu_location.getCurrentPosition(function(pos) {
					//alert(JSON.stringify(pos));
					var locObj = JSON.parse(pos);
          var map = new BMap.Map("allMap");
          //console.log(JSON.parse(pos));
          var point = new BMap.Point(locObj.longitude, locObj.latitude);
          map.centerAndZoom(point,18);//
          //向地图中添加缩放控件
          var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
          map.addControl(ctrl_nav);
          //向地图中添加缩略图控件
          var ctrl_ove = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:1});
          map.addControl(ctrl_ove);
          //向地图中添加比例尺控件
          var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
          map.addControl(ctrl_sca);
          map.addOverlay(new BMap.Marker(point));
				}, function(err) {
					alert(JSON.stringify(err));
				})
    }
  };
});
