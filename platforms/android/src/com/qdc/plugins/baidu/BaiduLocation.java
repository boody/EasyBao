package com.qdc.plugins.baidu;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;

/**
 * 百度云推送插件
 *
 * @author mrwutong
 *
 */
public class BaiduLocation extends CordovaPlugin {

    /** LOG TAG */
    private static final String LOG_TAG = BaiduLocation.class.getSimpleName();

    /** JS回调接口对象 */
    public static CallbackContext cbCtx = null;

    /** 百度定位客户端 */
    public LocationClient mLocationClient = null;

    /** 百度定位监听 */
    public BDLocationListener myListener = new BDLocationListener() {
        @Override
        public void onReceiveLocation(BDLocation location) {
            try {
                JSONObject json = new JSONObject();

                //json.put("time", location.getTime());
                json.put("locType", location.getLocType());
                json.put("latitude", location.getLatitude());
                json.put("longitude", location.getLongitude());
                json.put("radius", location.getRadius());

                if (location.getLocType() == BDLocation.TypeGpsLocation){// GPS定位结果
                    json.put("describe", "TypeGpsLocation");
                } else if (location.getLocType() == BDLocation.TypeNetWorkLocation){// 网络定位结果
                    json.put("describe", "TypeNetWorkLocation");
                } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
                    json.put("describe", "TypeOffLineLocation");
                } else if (location.getLocType() == BDLocation.TypeServerError) {
                    json.put("describe", "TypeServerError");
                } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
                    json.put("describe", "TypeNetWorkException");
                } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
                    json.put("describe", "TypeCriteriaException");
                }
    //            sb.append("\nlocationdescribe : ");
    //            sb.append(location.getLocationDescribe());// 位置语义化信息
    //            List<Poi> list = location.getPoiList();// POI数据
    //            if (list != null) {
    //                sb.append("\npoilist size = : ");
    //                sb.append(list.size());
    //                for (Poi p : list) {
    //                    sb.append("\npoi= : ");
    //                    sb.append(p.getId() + " " + p.getName() + " " + p.getRank());
    //                }
    //            }
                LOG.i(LOG_TAG, json.toString());

                PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, json.toString());
                pluginResult.setKeepCallback(true);
                cbCtx.sendPluginResult(pluginResult);
            } catch (JSONException e) {
                String errMsg = e.getMessage();
                LOG.e(LOG_TAG, errMsg, e);

                PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, errMsg);
                pluginResult.setKeepCallback(true);
                cbCtx.sendPluginResult(pluginResult);
            } finally {
                mLocationClient.stop();
            }
        }
    };

    /**
     * 插件主入口
     */
    @Override
    public boolean execute(String action, final JSONArray args, CallbackContext callbackContext) throws JSONException {
        LOG.d(LOG_TAG, "BaiduPush#execute");

        boolean ret = false;

        if ("getCurrentPosition".equalsIgnoreCase(action)) {
            cbCtx = callbackContext;

            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            cbCtx.sendPluginResult(pluginResult);

            if (mLocationClient == null) {
                mLocationClient = new LocationClient(this.webView.getContext());
                mLocationClient.registerLocationListener(myListener);

                // 配置定位SDK参数
                initLocation();
            }

            mLocationClient.start();
            ret = true;
        }

        return ret;
    }

    /**
     * 配置定位SDK参数
     */
    private void initLocation() {
        LocationClientOption option = new LocationClientOption();
        // 可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setLocationMode(LocationMode.Hight_Accuracy);
        // 可选，默认gcj02，设置返回的定位结果坐标系
        option.setCoorType("bd09ll");
        // 可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        // option.setScanSpan(0);
        // 可选，设置是否需要地址信息，默认不需要
        // option.setIsNeedAddress(false);
        // 可选，默认false,设置是否使用gps
        option.setOpenGps(true);
        // 可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setLocationNotify(false);
        /* 可选，默认false，设置是否需要位置语义化结果，
         * 可以在BDLocation.getLocationDescribe里得到，
         * 结果类似于“在北京天安门附近”
         */
        // option.setIsNeedLocationDescribe(true);
        // 可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        // option.setIsNeedLocationPoiList(true);
        // 可选，默认false，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认杀死
        // option.setIgnoreKillProcess(false);
        // 可选，默认false，设置是否收集CRASH信息，默认收集
        // option.SetIgnoreCacheException(true);
        // 可选，默认false，设置是否需要过滤gps仿真结果，默认需要
        // option.setEnableSimulateGps(false);
        mLocationClient.setLocOption(option);
    }
}
