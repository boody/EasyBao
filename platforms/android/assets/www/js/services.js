angular.module('starter.services', [])

.factory('Camera', ['$q', function($q) {
  return {
    getPicture: function(options) {
      var q = $q.defer();
      navigator.camera.getPicture(function(result) {
        q.resolve(result);
      }, function(err) {
        q.reject(err);
      }, options);

      return q.promise;
    },

    toBase64Image: function (img_path) {
      var q = $q.defer();
      window.imageResizer.resizeImage(function (success_resp) {
        q.resolve(success_resp);
      }, function (fail_resp) {
        q.reject(fail_resp);
      }, img_path, 200, 0, {
         imageDataType: ImageResizer.IMAGE_DATA_TYPE_URL,
         resizeType: ImageResizer.RESIZE_TYPE_MIN_PIXEL,
         pixelDensity: true,
         storeImage: false,
         photoAlbum: false,
         format: 'jpg'
      });

      return q.promise;
    }
  }
}])

.factory('Chats', function($http, $rootScope) {
  // Might use a resource here that returns a JSON array
  // Some fake testing data
  var chats = [];

  return {
    all: function() {
      return $http.get("http://222.92.142.130:8088/api/order/getOrdersByAccount/" + $rootScope.accId).then(function(response){
        chats = response.data;
        console.log("get data via " + $rootScope.accId);
        return chats;
      });
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})

.factory('Orders', function($http, $rootScope) {
  // Might use a resource here that returns a JSON array
  // Some fake testing data
  var orders = [];

  return {
    all: function() {
      return $http.get("http://222.92.142.130:8088/api/order/getFixsByAccount/" + $rootScope.accId).then(function(response){
        orders = response.data;
        console.log("get data via " + $rootScope.accId);
        return orders;
      });
    },
    remove: function(order) {
      orders.splice(orders.indexOf(order), 1);
    },
    get: function(orderId) {
      for (var i = 0; i < orders.length; i++) {
        if (orders[i].id === parseInt(orderId)) {
          return orders[i];
        }
      }
      return null;
    }
  };
});